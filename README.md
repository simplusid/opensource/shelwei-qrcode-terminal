# PHP QRCode Terminal

Generate QR code's for in your terminal. like https://github.com/gtanner/qrcode-terminal

![Basic Usage](example/basic.png)

> This library is re-hosted in here, because the original author has removed this library

# Install using `composer`:

    $ composer require simplusid/qrcode-terminal



## Usage

    $qrcode = \Simplusid\QRCode::terminal('Some data here');


# Command-Line
## Install

    $ composer global require shelwei/qrcode-terminal

## Usage

    $ qrcode-terminal --help
    $ qrcode-terminal 'https://github.com'

# Support

- OS X
- Linux


[basic-example-img]: https://raw.github.com/shelwei/qrcode-terminal/master/example/basic.png
